const express = require('express'); 
const app = express()

const swaggerUi = require('swagger-ui-express')
const swaggerDoc= require('./swagger.json');
app.get('/',(req,res)=>{
    res.send('Hi')
})

app.get('/test',(req,res)=>{
    res.send('Test')
})
app.use(
    '/api-docs',
    swaggerUi.serve, 
    swaggerUi.setup(swaggerDoc)
  );

app.listen(3000,()=>console.log('http://localhost:3000'))